/**
* Description of the Controller and the logic it provides
*
* @module  controllers/EmailRegistrationPipeline
*/

'use strict';

 var ISML = require('dw/template/ISML')
var guard = require('sheikh_controllers/cartridge/scripts/guard');
var transaction = require('dw/system/Transaction');
var CustomObjMgr = require('dw/object/CustomObjectMgr');



	function start() {
	
	
	var regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	var emailValid = request.httpParameterMap.get('email-text');
	var message = "";
	var button = false;
	
	 if( (emailValid!="") && regex.test(emailValid)==true ) {
	
	  if(CustomObjMgr.getCustomObject("EmailRegistration",emailValid)==null)
{
	transaction.wrap(function(){
		CustomObjMgr.createCustomObject("EmailRegistration",emailValid)
		message ="Thank you!"
		button= true
	})
}      
	else{
		message="email already exist"
		button= true
	}
}
    else{
        message="enter valid email address"
        button= true
}

ISML.renderTemplate('content/home/homepage.isml',{
	Button : button,
	Message:message
	
});
}


exports.Start = guard.ensure(['post'], start);