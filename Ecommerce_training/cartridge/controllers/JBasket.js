/**
* Description of the Controller and the logic it provides
*
* @module  controllers/JBasket
*/

'use strict';

var ISML =require('dw/template/ISML')         /* get ISML object from dw.template package */
var guard = require('sheikh_controllers/cartridge/scripts/guard')
var BasketMgr =require('dw/order/BasketMgr') /* get BasketMgr from dw.order package */

function start() {
var basket=BasketMgr.getCurrentBasket();

ISML.renderTemplate('showBasket.isml',
{
 Basket:basket
}
)
}

exports.Start=guard.ensure(['get'],start)