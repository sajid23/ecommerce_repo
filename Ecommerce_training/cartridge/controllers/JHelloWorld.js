/**
* Description of the Controller and the logic it provides
*
* @module  controllers/JHelloWorld
*/

'use strict';

 var guard = require('sheikh_controllers/cartridge/scripts/guard')
 var ISML = require('dw/template/ISML')
 
 function start() {
  
  ISML.renderTemplate(
                 'helloworld1.isml',
                 {
                   myParameterISML:'Hello from Controllers'
                 }
                 )
              }
  exports.Start=guard.ensure(['get'],start)
